package com.cursospring.springwebflux.controller;

import com.cursospring.springwebflux.document.Playlist;
import com.cursospring.springwebflux.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.Duration;

@RestController
@RequestMapping("/")
public class PlaylistController {

    private PlaylistService service;

    @Autowired
    public void setService(PlaylistService service) {
        this.service = service;
    }

    @GetMapping(value = "playlist")
    public Flux<Playlist> getPlaylistAll() {
        return service.findAll();
    }

    @GetMapping(value = "playlist/{id}")
    public Mono<Playlist> getPlaylistById(@PathVariable String id) {
        return service.findById(id);
    }

    @PostMapping(value = "playlist")
    public Mono<Playlist> savePlaylist(@RequestBody Playlist playlist) {
        return service.save(playlist);
    }

    @GetMapping(value = "playlist/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Tuple2<Long, Playlist>> getPlaylistByEvents() {
        Flux<Long> interval = Flux.interval(Duration.ofSeconds(10));
        Flux<Playlist> events = service.findAll();
        return Flux.zip(interval, events);
    }
}
